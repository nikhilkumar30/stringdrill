// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

// create function 
function convertToString(inputArray) {

    // create empty string
    let resultStr="";

    // for loop to store array into string 
    for (let index = 0; index < inputArray.length; index++) {
        resultStr +=inputArray[index];

        if (index<inputArray.length-1) {
            resultStr += " ";
        }
        
    }
    resultStr+="."

    return resultStr;
}

module.exports=convertToString;