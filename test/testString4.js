// import function

const titleCase=require("../string4");

// create object for test case  
let name={
    "first_name":"JoHN",
    "middle_name":"doe",
    "last_name":"SMith"
};

// call the function
let resultName=titleCase(name);

console.log(resultName);