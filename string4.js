// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

// create function
function titleCase(name) {
  // create variables and call the elements and convert it into  given format
  let firstName = capitalAndLowerLetter(name.first_name);
  let middleName = capitalAndLowerLetter(name.middle_name);
  let lastName = capitalAndLowerLetter(name.last_name);

  // for concating
  let fullName = firstName;

  if (middleName) {
    fullName += " " + middleName;
  }

  fullName += " " + lastName;

  return fullName;
}

// function to convert upper and lower case according to given format
function capitalAndLowerLetter(str) {
  return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}

module.exports = titleCase;
