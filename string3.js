// ==== String Problem #3 ====
// Given a string in the format of "20/1/2021", print the month in which the date is present in.

// create a function
function getMonthFromDate(inputDate) {
  // split date string by "/"
  let dateString = inputDate.split("/");

  // store the month and convert it into number
  let monthIndex = Number(dateString[1]);

  // create array in which months are written
  let monthsArray = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  // condition for month digit is valid or not and print the month Name
  if (monthIndex >= 1 && monthIndex <= 12) {
    let monthName = monthsArray[monthIndex - 1];
    console.log(monthName);
  } else {
    console.log("Invalid month index");
  }
}

module.exports = getMonthFromDate;
