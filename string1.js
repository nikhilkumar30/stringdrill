// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0.

// create the dunction
function convertStringToNumeric(inputString) {
  // Remove symbol at first index
  let removeSymbol = inputString.slice(1);

  // remove "," from new string
  let str = removeSymbol.replace(",", "");

  // convert the str into number
  let num = Number(str);

  return num;
}

// export the file
module.exports = convertStringToNumeric;
