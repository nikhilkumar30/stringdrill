// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

// create a function
function convertIpToNumbericArray(inputIp) {
  // split the component by "."
  let replaceDot = inputIp.split(".");

  // let an empty array to store
  let arrayNumber = [];

  // use for loop and convert splited strings into numbers
  for (let index = 0; index < replaceDot.length; index++) {
    let num = Number(replaceDot[index]);

    // check its number or not then push it into empty array
    if (!isNaN(num)) {
      arrayNumber.push(num);
    }
  }

  return arrayNumber;
}

module.exports = convertIpToNumbericArray;
